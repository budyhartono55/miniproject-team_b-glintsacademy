const { User, Role, Review } = require("../models");
const bcrypt = require("bcrypt");
const routes = {};
const jwt = require("jsonwebtoken");

//for get all user
routes.getAllMembers = async (req, res) => {
  try {
    const allUser = await User.findAll();
    res.send({
      statusCode: 200,
      statusText: "success",
      message: " Your request for GET ALL Members successfully",
      data: allUser,
    });
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

routes.getAllActivity = async (req, res) => {
  try {
    const allUser = await User.findAll({ include: [Review, Role] });
    res.send({
      statusCode: 200,
      statusText: "success",
      message: " Your request for GET ALL members activity successfully",
      data: allUser,
    });
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};
//===============================REGISTER====================================
routes.signUp = async (req, res) => {
  const reqBoPass = req.body.password;
  try {
    // const encryptPassword = (password) => {
    //   const encryptPassword = bcrypt.hashSync(password, 10);
    //   return encryptPassword;
    // };
    const encryptUserPass = { ...req.body, password: bcrypt.hashSync(reqBoPass, 10) };
    await User.create(encryptUserPass);

    //if user login success server will be show all details about user
    res.send({
      statusCode: 200,
      statusText: "success",
      message: " Your request for Sign Up successfully",
      data: encryptUserPass,
    });
    //if error signup
  } catch (error) {
    res.status(500).json({
      statusText: "SignUp Error!",
      message: error.message,
    });
  }
};

//===============================LOGIN===================================
//user login should be need fill, username, email and password
routes.signIn = async (req, res, next) => {
  try {
    //verify username
    //getting username form body and compare with username on database
    const { userName } = req.body;
    const verifyUsername = await User.findOne({ where: { userName: userName } });

    //If username doesn't match
    if (!verifyUsername) {
      return res.status(401).json({
        message: "Invalid username",
      });
    }
    //getting email form body and compare with email on database
    const { email } = req.body;
    const user = await User.findOne({ where: { email: email } });

    //if email doesn't exist
    if (!user) {
      return res.status(401).json({
        message: "Email not registered!",
      });
    }

    // verify the password
    const verifyPassword = await bcrypt.compare(req.body.password, user.password);
    // If password doesn't match
    if (!verifyPassword) {
      return res.status(401).json({
        message: "Wrong Password",
      });
    }

    //next to GET TOKEN
    next();

    //if login Error
  } catch (error) {
    console.log(error);
    res.json({
      statusCode: 400,
      statusText: "Error",
      message: error,
    });
  }
};

module.exports = routes;
