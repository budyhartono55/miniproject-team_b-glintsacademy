const { Role } = require("../models");
const routes = {};

routes.postRole = async (req, res) => {
  try {
    const role = req.body;
    await Role.create(role);
    res.send({
      statusCode: 200,
      statusText: "success",
      message: " Your request for POST Role's successfully",
      data: role,
    });
  } catch (error) {
    res.status(500).json(error);
  }
};

module.exports = routes;
