const { Review } = require("../models");
const routes = {};

routes.postReview = async (req, res) => {
  try {
    const review = { ...req.body };
    await Review.create(review);
    res.send({
      statusCode: 200,
      statusText: "success",
      message: " Your request for POST Review's successfully",
      data: review,
    });
  } catch (error) {
    res.status(500).json(error);
  }
};

module.exports = routes;
