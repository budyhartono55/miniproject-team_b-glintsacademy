"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  //User CLASS
  class User extends Model {
    static associate(models) {
      // one User just can give only One review
      this.hasOne(models.Review, { foreignKey: "userId" });
      //getting role for user from Role CLASS
      this.belongsTo(models.Role, { foreignKey: "roleId" });
    }
  }
  User.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      //required
      fullName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      //unique
      userName: {
        type: DataTypes.STRING,
        unique: true,
      },
      //required & unique
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      profilPicture: {
        type: DataTypes.STRING,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      //required
      roleId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        foreignKey: true,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: new Date(),
      },
    },
    {
      sequelize,
      modelName: "User",
      tableName: "users",
    }
  );
  return User;
};
