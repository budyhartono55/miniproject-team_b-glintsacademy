const express = require("express");

const router = express.Router();
const { postRole } = require("../controllers/roleControllers");

router.post("/post/role", postRole);

module.exports = router;
