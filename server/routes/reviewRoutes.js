const express = require("express");

const router = express.Router();
const { postReview } = require("../controllers/reviewControllers");

router.post("/post/review", postReview);

module.exports = router;
