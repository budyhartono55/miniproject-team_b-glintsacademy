const express = require("express");

const router = express.Router();
const { signUp, signIn, getAllActivity, getAllMembers } = require("../controllers/userControllers");
const { getToken, verifyToken } = require("../middleware/auth");

router.get("/members/activity", getAllActivity);
router.get("/members", getAllMembers);
router.post("/signUp", signUp);

router.post("/login", signIn, getToken);
router.get("/verify", verifyToken);

module.exports = router;
