//AUTHENTICATION

const jwt = require("jsonwebtoken"); //import jwt
const bcrypt = require("bcrypt"); //import bcrypt for encrypt password
const { User } = require("../models"); // Import user model
routes = {};

routes.getToken = async (req, res) => {
  try {
    const { email } = req.body;
    const user = await User.findOne({ where: { email: email } });

    const token = jwt.sign(
      {
        email: user.dataValues.email,
      },
      process.env.SECRET_KEY
    );

    const userResult = {
      statusCode: 200,
      statusText: "Success",
      message: "Login Success!",
      data: {
        token: token,
        data: user.dataValues,
      },
    };
    res.json(userResult);
  } catch (error) {
    console.log(error);
    res.json({
      statusCode: 400,
      statusText: "Error",
      message: error,
    });
  }
};

routes.verifyToken = async (req, res) => {
  try {
    const token = req.headers.authorization;
    const { email } = req.body;
    const user = await User.findOne({ where: { email: email } });
    jwt.verify(token, process.env.SECRET_KEY, (err) => {
      if (err) {
        res.json(err);
      }
      if (!user) {
        res.json({
          statusCode: 401,
          statusText: "user not found!",
          message: "invalid email,please check your email!",
        });
      } else {
        res.json({
          statusCode: 200,
          statusText: "Success!",
          message: "hi!,Your request for verify user Successfully",
          data: user,
        });
      }
    });
  } catch (error) {
    console.log(error);

    res.json({
      statusCode: 400,
      statusText: "Error",
      message: error,
    });
  }
};

module.exports = routes;
