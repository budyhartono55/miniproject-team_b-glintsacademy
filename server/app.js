//================================BASIC-STRUCTURE===========================================

// import .env
require("dotenv").config();
//express modules
const express = require("express");

//parsing
//const bodyParser = require("body-parser"); //bp modules

// import morgan modules
const logger = require("morgan");
//import cors modules
const cors = require("cors");

//this actions for client
var cors_options = {
  origin: `http://localhost:${process.env.SERVER_PORT}`,
};

const PORT = process.env.SERVER_PORT;
const app = express();

app.use(cors(cors_options));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(logger("dev"));

//================================ROUTE===========================================

//MAIN ROUTES {userRoutes}
const userRoutes = require("./routes/userRoutes");
const roleRoutes = require("./routes/roleRoutes");
const reviewRoutes = require("./routes/reviewRoutes");

app.use("/API/v1.0/r_movie", userRoutes, roleRoutes, reviewRoutes);

//default routes for test connection
app.get("/", (req, res) => {
  res.send({
    status_code: 200,
    status_message: "Success",
    message: "Test Connection Success!",
  });
});

//================================SERVER===========================================
//if user fill routes doesn"t exist wil be return it.
app.all("*", (req, res) => res.send("Route don't exist"));

//connection
app.listen(PORT, (err) => {
  //if connection error will be send respon error and console will be return it
  if (err) {
    res.send(err);
    console.log("Connection Failed!");
  }
  //if connection success
  console.log(`DEFAULT route Server Running on | http://localhost:${PORT}`);
});

//Double Test Connections for database
const db_config = require("./config/database");
db_config
  .authenticate()
  .then(() => console.log("Server Connected..."))
  .catch((error) => console.log("Connection Failed!", error));
