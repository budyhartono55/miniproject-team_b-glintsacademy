# BackendTeamB

# API List

| Routes | EndPoint                           | Description                                                   |
| ------ | ---------------------------------- | ------------------------------------------------------------- |
|        | API members                        | DETAILS :                                                     |
| ------ | ---------------------------------- | ------------------------------------------------------------- |
| GET    | /API/v1.0/r_movie/members/         | show all members                                              |
| GET    | /API/v1.0/r_movie/members/activity | to get all members activity                                   |
| POST   | /API/v1.0/r_movie/signUp           | Register members                                              |
| POST   | /API/v1.0/r_movie/login            | Get Token & Login members with {username, email and password} |
| GET    | /API/v1.0/r_movie/verify           | to verify members with {token->(header) and email->(body)}    |
| ------ | ---------------------------------- | ------------------------------------------------------------- |
|        | API role                           | DETAILS :                                                     |
| ------ | ---------------------------------- | ------------------------------------------------------------- |
| POST   | /API/v1.0/r_movie/post/role        | Post Role members (developer only !)                          |
| ------ | ---------------------------------- | ------------------------------------------------------------- |
|        | API review                         | DETAILS                                                       |
| ------ | ---------------------------------- | ------------------------------------------------------------- |
| POST   | /API/v1.0/r_movie/post/review      | to Post Review and rating for movie (members)                 |

---

# How to run

## Server

```bash
$ npm start
```
